SET FOREIGN_KEY_CHECKS=0;

START TRANSACTION;

-- 贴吧板块升级
DELETE FROM `tn_Sections`;
INSERT `tn_Sections` (
       `SectionId`
      ,`TenantTypeId`
      ,`OwnerId`
      ,`UserId`
      ,`Name`
      ,`Description`
      ,`FeaturedImageAttachmentId`
      ,`IsEnabled`
      ,`ThreadCategorySettings`
      ,`DisplayOrder`
      ,`DateCreated`
      ,`PropertyNames`
      ,`PropertyValues`)
SELECT 
       `SectionId`
      ,'100003' as `TenantTypeId`
      ,`OwnerId`
      ,`UserId`
      ,`Name`
      ,`Description`
      ,0 as `FeaturedImageAttachmentId`
      ,`IsEnabled`
      ,`ThreadCategoryStatus` as `ThreadCategorySettings`
      ,`DisplayOrder`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
      ,`PropertyNames`
      ,`PropertyValues`
  FROM `tn.jinhudemoNew`.`spb_BarSections`;

-- 贴子升级
DELETE FROM `tn_Threads`;

INSERT `tn_Threads` (
       `ThreadId`
      ,`SectionId`
      ,`TenantTypeId`
      ,`OwnerId`
      ,`UserId`
      ,`Author`
      ,`Subject`
      ,`Body`
      ,`IsLocked`
      ,`IsSticky`
      ,`ApprovalStatus`
      ,`IP`
      ,`DateCreated`
      ,`LastModified`
      ,`PropertyNames`
      ,`PropertyValues`)
SELECT  `ThreadId`
      ,`SectionId`
      ,'100002' as `TenantTypeId`
      ,`OwnerId`
      ,`UserId`
      ,`Author`
      ,`Subject`
      ,`Body`
      ,`IsLocked`
	   ,`IsSticky`
      ,`AuditStatus` as `ApprovalStatus`
      ,`IP`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
      ,date_sub(`LastModified`,interval -8 hour)  as `LastModified`
      ,`PropertyNames`
      ,`PropertyValues`
  FROM `tn.jinhudemoNew`.`spb_BarThreads`;



 INSERT `tn_OperationLogs` ( `TenantTypeId`, `OperationType`, `OperationObjectId`, `OperationObjectName`, `Description`, `OperationUserId`, `Operator`, `OperatorIP`, `AccessUrl`, `DateCreated`) 
SELECT 
	   CASE  `Source` 
	   WHEN  '贴吧' THEN '100003' 
	   WHEN  '贴子' THEN '100002'
	   WHEN  '资讯' THEN '100011'
	   WHEN  '微博' THEN '100011'
	   WHEN  '评论' THEN '000031'
	   ELSE `Source` END as `TenantTypeId`
      ,`OperationType`
      ,`OperationObjectId`
	  ,`OperationObjectName`
      ,`Description`
	 
      ,`OperatorUserId`
      ,`Operator`
      ,`OperatorIP`
      ,`AccessUrl`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
  FROM `tn.jinhudemoNew`.`tn_OperationLogs`
  where  `Source` = '帖吧' or `Source` = '帖子';

    -- 附件升级

INSERT `tn_Attachments` (`AttachmentId`, `AssociateId`, `OwnerId`, `TenantTypeId`, `UserId`, `UserDisplayName`, `FileName`, `FriendlyFileName`, `MediaType`, `ContentType`, `FileLength`, `Price`, `IP`, `ConvertStatus`, `DateCreated`, `IsShowInAttachmentList`, `PropertyNames`, `PropertyValues`, `DisplayOrder`) 
SELECT `AttachmentId`
      ,`AssociateId`
      ,`OwnerId`
      ,
	   CASE  `TenantTypeId` 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100001' 
	   WHEN  '101501' THEN '100011' 
	   WHEN  '100101' THEN '100011' 
	   ELSE `TenantTypeId` END as `TenantTypeId`
      ,`UserId`
      ,`UserDisplayName`
      ,`FileName`
      ,`FriendlyFileName`
      ,`MediaType`
      ,`ContentType`
      ,`FileLength`
      ,`Price`
      ,`IP`
	  ,0 as `ConvertStatus`
      ,`DateCreated`
	 
	  ,0 as `IsShowInAttachmentList`
      ,`PropertyNames`
      ,`PropertyValues`
	  ,`AttachmentId` as `DisplayOrder`
  FROM `tn.jinhudemoNew`.`tn_Attachments` where `TenantTypeId` in('101201','101202') ;
  

     -- 收藏升级
  insert `tn_Favorites`( 
      `TenantTypeId`
      ,`UserId`
      ,`ObjectId`)
SELECT   
       CASE  `TenantTypeId` 
	   WHEN  '101202' THEN '100002'
	   WHEN  '101201' THEN '100003'  
	   WHEN  '101501' THEN '100011' 
	   WHEN  '100101' THEN '100011' 
	   ELSE `TenantTypeId` END as `TenantTypeId`
      ,`UserId`
      ,`ObjectId`
  FROM `tn.jinhudemoNew`.`tn_Favorites` where `TenantTypeId` in('101201','101202') ;

   -- 评论升级
INSERT `tn_Comments` (`Id`
      ,ParentIds
      ,`ParentId`
      ,`CommentedObjectId`
      ,`TenantTypeId`
     
      ,`ChildrenCount`
      ,`OwnerId`
      ,`UserId`
      ,`Author`
      ,`Subject`
      ,`Body`
      ,`IsAnonymous`
      ,`IsPrivate`
      ,`ApprovalStatus`
      ,`IP`
      ,`DateCreated`
     )
SELECT 
       postid+1000000 as Id,
      CASE  
	   WHEN  `ParentId`>0 THEN CONCAT('0,',`ParentId`+1000000,',')
	   ELSE '0,' END as `ParentIds`,
	   CASE  
	   WHEN  `ParentId`>0 THEN `ParentId`+1000000
	   ELSE ParentId END as `ParentId`,
	    ThreadId as `CommentedObjectId`
      ,'100002' as `TenantTypeId`
      
       ,`ChildPostCount` as `ChildrenCount`
      ,`OwnerId`
      ,`UserId`
      ,`Author`
      ,`Subject`
      ,`Body`
      ,0 as `IsAnonymous`
      ,0 as `IsPrivate`
      ,`AuditStatus` as `ApprovalStatus`
      ,`IP`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
  FROM `tn.jinhudemoNew`.`spb_BarPosts`;

  -- 点赞升级

INSERT `tn_Attitudes` (`Id`
      , `TenantTypeId`
      ,`ObjectId`
      ,`SupportCount`)
SELECT `Id`
       , CASE  `TenantTypeId` 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	    WHEN  '100101' THEN '100011' 
	   ELSE `TenantTypeId` END as `TenantTypeId`
      ,`ObjectId`
      ,`SupportCount`
  FROM `tn.jinhudemoNew`.`tn_Attitudes` where `TenantTypeId` in('101201','101202') ;

-- 点赞记录升级

INSERT `tn_AttitudeRecords` (`Id`
      ,`TenantTypeId`
      ,`ObjectId`
      ,`UserId`)
SELECT  `Id`
      ,CASE  `TenantTypeId` 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	    WHEN  '100101' THEN '100011' 
	   ELSE `TenantTypeId` END as `TenantTypeId`
      ,`ObjectId`
      ,`UserId`
  FROM `tn.jinhudemoNew`.`tn_AttitudeRecords` where `TenantTypeId` in('101201','101202');




-- 类别升级

INSERT `tn_Categories` (`CategoryId`
      ,`ParentId`
      ,`OwnerId`
      ,`TenantTypeId`
      ,`CategoryName`
      ,`Description`
      ,`DisplayOrder`
      ,`Depth`
      ,`ChildCount`
      ,`ItemCount`
      ,`ImageAttachmentId`
      ,`LastModified`
      ,`DateCreated`
      ,`PropertyNames`
      ,`PropertyValues`)
SELECT `CategoryId`
      ,`ParentId`
      ,`OwnerId`
      ,CASE  `TenantTypeId` 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	    WHEN  '100101' THEN '100011' 
	   ELSE `TenantTypeId` END as `TenantTypeId`
      ,`CategoryName`
      ,`Description`
      ,`DisplayOrder`
      ,`Depth`
      ,`ChildCount`
      ,`ItemCount`
      ,0 as `ImageAttachmentId`
      ,date_sub(`LastModified`,interval -8 hour)  as `LastModified`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
      ,`PropertyNames`
      ,`PropertyValues`
  FROM `tn.jinhudemoNew`.`tn_Categories` where `TenantTypeId` in('101201','101202');

-- 类别和内容关联升级

INSERT `tn_ItemsInCategories` (`Id`
      ,`CategoryId`
      ,`ItemId`)
SELECT  `Id`
      ,`tn_ItemsInCategories`.`CategoryId`
      ,`ItemId`
  FROM `tn.jinhudemoNew`.`tn_ItemsInCategories` left join `tn.jinhudemoNew`.`tn_Categories`  on  `tn.jinhudemoNew`.`tn_Categories`.`CategoryId`  =`tn_ItemsInCategories`.`CategoryId` where  `tn.jinhudemoNew`.`tn_Categories`.TenantTypeId in('101201','101202');



-- 标签升级

INSERT `tn_Tags` (`TagId`
      ,`TenantTypeId`
      ,`TagName`
      ,`Description`
      ,`ImageAttachmentId`
      ,`IsFeatured`
      ,`ItemCount`
      ,`DateCreated`
      ,`PropertyNames`
      ,`PropertyValues`)
SELECT `TagId`
      ,CASE  `TenantTypeId` 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	   WHEN  '100101' THEN '100011' 
	   ELSE `TenantTypeId` END as `TenantTypeId`
      ,`TagName`
      ,`Description`
      ,0 as  `ImageAttachmentId`
      ,`IsFeatured`
      ,`ItemCount`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
      ,`PropertyNames`
      ,`PropertyValues`
  FROM `tn.jinhudemoNew`.`tn_Tags` where `TenantTypeId` in('101201','101202') ;


-- 标签关联项升级

INSERT `tn_ItemsInTags` (`Id`
      ,`TagName`
      ,`ItemId`
      ,`TenantTypeId`)
SELECT  `Id`
      ,`TagName`
      ,`ItemId`
      ,CASE  `TenantTypeId` 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	   ELSE `TenantTypeId` END as `TenantTypeId`
  FROM `tn.jinhudemoNew`.`tn_ItemsInTags` where `TenantTypeId` in('101201','101202') ;



-- 栏目管理员升级
DELETE FROM `tn_CategoryManagers`;

INSERT `tn_CategoryManagers` (`Id`
      ,`CategoryId`
      ,`TenantTypeId`
      ,`ReferenceCategoryId`
      ,`UserId`)
SELECT `Id`
      ,`SectionId` as `CategoryId`
	  ,'100001' as `TenantTypeId`
	  ,0 as `ReferenceCategoryId`
      ,`UserId`
  FROM `tn.jinhudemoNew`.`spb_BarSectionManagers`;





-- 贴子精华升级
INSERT `tn_SpecialContentItems` (
       `TenantTypeId`
      ,`TypeId`
      ,`RegionId`
      ,`ItemId`
      ,`ItemName`
      ,`FeaturedImageAttachmentId`
     
      ,`RecommenderUserId`
      ,`DateCreated`
      ,`ExpiredDate`
      ,`DisplayOrder`
  )
SELECT  
      '100002' as `TenantTypeId`
      ,11 as `TypeId`
      ,0 as `RegionId`
      ,`ThreadId` as  `ItemId`
	  ,`Subject` as `ItemName`
      , 0 as `FeaturedImageAttachmentId`
    
	  ,0 as `RecommenderUserId`
      , date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
	  ,date_sub(NOW(),interval -20 year)  as `ExpiredDate`
	  , `ThreadId` as  `DisplayOrder`
  FROM `tn.jinhudemoNew`.`spb_BarThreads` where `IsEssential`=1;


    -- 贴吧关联项升级
delete from `tn_SearchWords`  where SearchTypeCode = 'Thread';
INSERT .`tn_SearchWords` (`id`,
       `Word`
      ,`SearchTypeCode`
      ,`IsAddedByAdministrator`
      ,`DateCreated`
      ,`LastModified`)
	  SELECT  `id`,
      `Term` as `Word`
      ,'Thread' as `SearchTypeCode`
      ,`IsAddedByAdministrator`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
      ,`LastModified`
  FROM `tn.jinhudemoNew`.`tn_SearchedTerms` where SearchTypeCode = 'BarSearcher';
   COMMIT;