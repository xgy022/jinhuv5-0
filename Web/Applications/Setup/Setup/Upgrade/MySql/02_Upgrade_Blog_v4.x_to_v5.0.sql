SET FOREIGN_KEY_CHECKS=0;

START TRANSACTION;
-- 内容项升级
INSERT `tn_ContentItems` (
       `ContentItemId`
       ,`ContentCategoryId`
      ,`ContentModelId`
      ,`Subject`
      ,`FeaturedImageAttachmentId`
     
      ,`Points`
      ,`UserId`
      ,`Author`
      ,`Body`
     ,`Summary`
      ,`IsLocked`
      ,`IsSticky`
      ,`ApprovalStatus`
      ,`IP`
      ,`DatePublished`
      ,`DateCreated`
      ,`LastModified`
     ,`PropertyNames`
      ,`PropertyValues`)
SELECT 
`ThreadId`+1000000 as `ContentItemId`,
       1 as `ContentCategoryId`
       ,5  as `ContentModelId`
	   , `Subject`
      ,`FeaturedImageAttachmentId`
	  
	   ,0 as `Points`
	  ,`UserId`
      ,`Author`
      ,`Body`
	 ,`Summary`
	  , `IsLocked`
	    , `IsSticky`
      ,`AuditStatus` as `ApprovalStatus`
      ,`IP`
      ,date_sub(`DateCreated`,interval -8 hour)  as  `DatePublished`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
      ,date_sub(`DateCreated`,interval -8 hour)   as`LastModified`
      ,'IsVisible:S:0:4:' as `PropertyNames`
      ,'True' as `PropertyValues`
  FROM `tn.jinhudemoNew`.`spb_BlogThreads`;

  INSERT `tn_OperationLogs` ( `TenantTypeId`, `OperationType`, `OperationObjectId`, `OperationObjectName`, `Description`,  `OperationUserId`, `Operator`, `OperatorIP`, `AccessUrl`, `DateCreated`) 
SELECT 
	   CASE  `Source` 
	   WHEN  '贴吧' THEN '100003' 
	   WHEN  '贴子' THEN '100002'
	   WHEN  '资讯' THEN '100011'
	   WHEN  '微博' THEN '100011'
	   WHEN  '评论' THEN '000031'
	   ELSE `Source` END as `TenantTypeId`
      ,`OperationType`
      ,`OperationObjectId`
	  ,`OperationObjectName`
      ,`Description`
	 
      ,`OperatorUserId`
      ,`Operator`
      ,`OperatorIP`
      ,`AccessUrl`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
  FROM `tn.jinhudemoNew`.`tn_OperationLogs`
  where  `Source` = '微博';

   -- 附件升级

INSERT `tn_Attachments` (`AttachmentId`, `AssociateId`, `OwnerId`, `TenantTypeId`, `UserId`, `UserDisplayName`, `FileName`, `FriendlyFileName`, `MediaType`, `ContentType`, `FileLength`, `Price`, `IP`, `ConvertStatus`, `DateCreated`, `IsShowInAttachmentList`, `PropertyNames`, `PropertyValues`, `DisplayOrder`) 
SELECT `AttachmentId`
      ,`AssociateId`+1000000
      ,`OwnerId`
      ,
	   CASE  `TenantTypeId` 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	   WHEN  '100201' THEN '100011' 
	   ELSE `TenantTypeId` END as `TenantTypeId`
      ,`UserId`
      ,`UserDisplayName`
      ,`FileName`
      ,`FriendlyFileName`
      ,`MediaType`
      ,`ContentType`
      ,`FileLength`
      ,`Price`
      ,`IP`
	  ,0 as `ConvertStatus`
      , `DateCreated`
	  
	  ,0 as `IsShowInAttachmentList`
      ,`PropertyNames`
      ,`PropertyValues`
	  ,`AttachmentId` as `DisplayOrder`
  FROM `tn.jinhudemoNew`.`tn_Attachments` where `TenantTypeId` in('100201') ;
  

   -- 收藏升级
  insert `tn_Favorites`( 
      `TenantTypeId`
      ,`UserId`
      ,`ObjectId`)
SELECT   
       CASE  `TenantTypeId` 
	   WHEN  '101202' THEN '100002'
	   WHEN  '101201' THEN '100003'  
	   WHEN  '101501' THEN '100011' 
	   WHEN  '100201' THEN '100011' 
	   ELSE `TenantTypeId` END as `TenantTypeId`
      ,`UserId`
      ,`ObjectId`+1000000
  FROM `tn.jinhudemoNew`.`tn_Favorites` where `TenantTypeId` in('100201') ;

  -- 评论升级

INSERT `tn_Comments` (`Id`
      ,`ParentIds`
      ,`ParentId`
      ,`CommentedObjectId`
      ,`TenantTypeId`
     
      ,`ChildrenCount`
      ,`OwnerId`
      ,`UserId`
      ,`Author`
      ,`Subject`
      ,`Body`
      ,`IsAnonymous`
      ,`IsPrivate`
      ,`ApprovalStatus`
      ,`IP`
      ,`DateCreated`
      ,`PropertyNames`
      ,`PropertyValues`)
SELECT  `Id`
       ,CASE  
	   WHEN  `ParentId`>0 THEN CONCAT('0,',`ParentId`,',')
	   ELSE '0,' END as `ParentIds`
      ,`ParentId`
      ,`CommentedObjectId`+1000000
      , CASE  `TenantTypeId` 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	   WHEN  '100201' THEN '100011' 
	   ELSE `TenantTypeId` END as `TenantTypeId`
	 
	  ,`ChildCount` as `ChildrenCount`
      ,`OwnerId`
      ,`UserId`
      ,`Author`
      ,`Subject`
      ,`Body`
      ,`IsAnonymous`
      ,`IsPrivate`
      ,`AuditStatus` as `ApprovalStatus`
      ,`IP`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
      ,`PropertyNames`
      ,`PropertyValues`
  FROM `tn.jinhudemoNew`.`tn_Comments` where `TenantTypeId` in('100201') ;

  -- 点赞升级

INSERT `tn_Attitudes` (`Id`
      , `TenantTypeId`
      ,`ObjectId`
      ,`SupportCount`)
SELECT `Id`
       , CASE  `TenantTypeId` 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	    WHEN  '100201' THEN '100011' 
	   ELSE `TenantTypeId` END as `TenantTypeId`
      ,`ObjectId`+1000000
      ,`SupportCount`
  FROM `tn.jinhudemoNew`.`tn_Attitudes` where `TenantTypeId` in('100201') ;

-- 点赞记录升级

INSERT `tn_AttitudeRecords` (`Id`
      ,`TenantTypeId`
      ,`ObjectId`
      ,`UserId`)
SELECT  `Id`
      ,CASE  `TenantTypeId` 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	    WHEN  '100201' THEN '100011' 
	   ELSE `TenantTypeId` END as `TenantTypeId`
      ,`ObjectId`+1000000
      ,`UserId`
  FROM `tn.jinhudemoNew`.`tn_AttitudeRecords` where `TenantTypeId` in('100201') ;




-- 类别升级

INSERT `tn_Categories` (`CategoryId`
      ,`ParentId`
      ,`OwnerId`
      ,`TenantTypeId`
      ,`CategoryName`
      ,`Description`
      ,`DisplayOrder`
      ,`Depth`
      ,`ChildCount`
      ,`ItemCount`
      ,`ImageAttachmentId`
      ,`LastModified`
      ,`DateCreated`
      ,`PropertyNames`
      ,`PropertyValues`)
SELECT `CategoryId`
      ,`ParentId`
      ,`OwnerId`
      ,CASE  `TenantTypeId` 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	    WHEN  '100201' THEN '100011' 
	   ELSE `TenantTypeId` END as `TenantTypeId`
      ,`CategoryName`
      ,`Description`
      ,`DisplayOrder`
      ,`Depth`
      ,`ChildCount`
      ,`ItemCount`
      ,0 as `ImageAttachmentId`
      ,date_sub(`LastModified`,interval -8 hour)  as `LastModified`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
      ,`PropertyNames`
      ,`PropertyValues`
  FROM `tn.jinhudemoNew`.`tn_Categories` where `TenantTypeId` in('100201') ;


-- 类别和内容关联升级

INSERT `tn_ItemsInCategories` (`Id`
      ,`CategoryId`
      ,`ItemId`)
SELECT  `Id`
      ,`tn_ItemsInCategories`.`CategoryId`
      ,`ItemId`+1000000
  FROM `tn.jinhudemoNew`.`tn_ItemsInCategories` left join `tn.jinhudemoNew`.`tn_Categories`  on  `tn.jinhudemoNew`.`tn_Categories`.`CategoryId`  =`tn_ItemsInCategories`.`CategoryId` where  `tn.jinhudemoNew`.`tn_Categories`.TenantTypeId in('100201');





-- 标签升级

INSERT `tn_Tags` (`TagId`
      ,`TenantTypeId`
      ,`TagName`
      ,`Description`
      ,`ImageAttachmentId`
      ,`IsFeatured`
      ,`ItemCount`
      ,`DateCreated`
      ,`PropertyNames`
      ,`PropertyValues`)
SELECT `TagId`
      ,CASE  `TenantTypeId` 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	   WHEN  '100201' THEN '100011' 
	   ELSE `TenantTypeId` END as `TenantTypeId`
      ,`TagName`
      ,`Description`
      ,0 as  `ImageAttachmentId`
      ,`IsFeatured`
      ,`ItemCount`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
      ,`PropertyNames`
      ,`PropertyValues`
  FROM `tn.jinhudemoNew`.`tn_Tags` where `TenantTypeId` in('100201') ;


-- 标签关联项升级

INSERT `tn_ItemsInTags` (`Id`
      ,`TagName`
      ,`ItemId`
      ,`TenantTypeId`)
SELECT  `Id`
      ,`TagName`
      ,`ItemId`+1000000
      ,CASE  `TenantTypeId` 
	   WHEN  '101202' THEN '100002' 
	   WHEN  '101201' THEN '100003' 
	   WHEN  '101501' THEN '100011' 
	   ELSE `TenantTypeId` END as `TenantTypeId`
  FROM `tn.jinhudemoNew`.`tn_ItemsInTags` where `TenantTypeId` in('100201') ;


  -- 微博精华升级
INSERT `tn_SpecialContentItems` (
       `TenantTypeId`
      ,`TypeId`
      ,`RegionId`
      ,`ItemId`
      ,`ItemName`
      ,`FeaturedImageAttachmentId`
     
      ,`RecommenderUserId`
      ,`DateCreated`
      ,`ExpiredDate`
      ,`DisplayOrder`
  )
SELECT  
      '101501' as `TenantTypeId`
      ,11 as `TypeId`
      ,0 as `RegionId`
      ,`ThreadId`+1000000 as  `ItemId`
	  ,`Subject` as `ItemName`
      , 0 as `FeaturedImageAttachmentId`
    
	  ,0 as `RecommenderUserId`
      , date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
	  ,date_sub(NOW(),interval -20 year)  as `ExpiredDate`
	  , `ThreadId` as  `DisplayOrder`
  FROM `tn.jinhudemoNew`.`spb_blogthreads` where `IsEssential`=1;

   -- 微博热词搜索

INSERT .`tn_SearchWords` (`id`,
       `Word`
      ,`SearchTypeCode`
      ,`IsAddedByAdministrator`
      ,`DateCreated`
      ,`LastModified`)
	  SELECT  `id`,
      `Term` as `Word`
      ,'Cms' as `SearchTypeCode`
      ,`IsAddedByAdministrator`
      ,date_sub(`DateCreated`,interval -8 hour)  as `DateCreated`
      ,`LastModified`
  FROM `tn.jinhudemoNew`.`tn_SearchedTerms` where SearchTypeCode = 'BlogSearcher';


  -- 微博栏目内容项升级 

update  `tn_ContentCategories` set `ChildCount` = `ChildCount`+(SELECT count( `ThreadId`)
  FROM `tn.jinhudemoNew`.`spb_BlogThreads`)
 where  `CategoryId` =1;
    COMMIT;