﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

using System.ComponentModel.DataAnnotations;

namespace Tunynet.Common
{
    /// <summary>
    /// 举报的ViewModel
    /// </summary>

    public class ImpeachReportEditModel
    {
        /// <summary>
        ///租户Id
        /// </summary>
        public string TenantTypeId { get; set; }

        /// <summary>
        ///举报原因
        /// </summary>
        [Display(Name = "举报原因")]
        [Required(ErrorMessage = "请选择举报原因")]
        public ImpeachReasonEnum Reason { get; set; }

        /// <summary>
        ///举报对象标题
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        ///附加说明
        /// </summary>
        [Display(Name = "附加说明")]
        [StringLength(100, ErrorMessage = "字数不能大于100个字")]
        public string Description { get; set; }

        /// <summary>
        ///被举报相关对象Id
        /// </summary>
        public long ReportObjectId { get; set; }
    }
}