﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

using Tunynet.Post;
using Tunynet.Search;
using Tunynet.Tasks;

namespace Tunynet.Common
{
    /// <summary>
    /// 贴子索引任务
    /// </summary>
    public class ThreadSearchTask : ITask
    {
        /// <summary>
        /// 任务执行的内容
        /// </summary>
        /// <param name="taskDetail">任务配置状态信息</param>
        public void Execute(TaskDetail taskDetail)
        {
            ThreadSearcher threadSearcher = (ThreadSearcher)SearcherFactory.GetSearcher(ThreadSearcher.CODE);

            threadSearcher.SearchTask();
        }
    }
}