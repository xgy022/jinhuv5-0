﻿//------------------------------------------------------------------------------
// <copyright company="Tunynet">
//     Copyright (c) Tunynet Inc.  All rights reserved.
// </copyright> 
//------------------------------------------------------------------------------

using PetaPoco;
using System;
using System.Collections.Generic;
using System.Linq;
using Tunynet.Caching;
using Tunynet.Repositories;

namespace Tunynet.Common
{
    /// <summary>
    /// AttachmentAccessRecords仓储
    /// </summary>
    public class AttachmentAccessRecordsRepository<T> : Repository<AttachmentAccessRecords>, IAttachmentAccessRecordsRepository
    {
        private int pageSize = 20;

        #region Create/Update

        /// <summary>
        /// 创建新的下载记录
        /// </summary>
        /// <param name="record">下载记录实体</param>
        /// <returns>下载记录Id</returns>
        public new void Insert(AttachmentAccessRecords record)
        {
            base.Insert(record);
            if (record.Id > 0)
            {
                string cacheKey = GetCacheKey_RecordIds_AttachmentIds(record.UserId);
                Dictionary<long, long> ids_AttachmentIds = cacheService.GetFromFirstLevel<Dictionary<long, long>>(cacheKey);
                if (ids_AttachmentIds != null && !ids_AttachmentIds.Values.Contains(record.AttachmentId))
                {
                    ids_AttachmentIds[record.Id] = record.AttachmentId;
                    cacheService.Set(cacheKey, ids_AttachmentIds, CachingExpirationType.UsualObjectCollection);
                }
            }
        }

        /// <summary>
        /// 更新最后下载时间
        /// </summary>
        /// <param name="userId">下载用户UserId</param>
        /// <param name="attachmentId">附件Id</param>
        /// <param name="accesstype">操作类型</param>
        public bool UpdateLastDownloadDate(long userId, long attachmentId, AccessType accesstype = AccessType.Download)
        {
            var sql = Sql.Builder;
            sql.Append("Update tn_AttachmentAccessRecords set LastDownloadDate = @0", DateTime.Now)
               .Where("UserId = @0 and AttachmentId = @1 and AccessType=@2", userId, attachmentId, accesstype);

            int count = CreateDAO().Execute(sql);

            Dictionary<long, long> ids_AttachmentIds = GetIds_AttachmentIdsByUser(userId);
            if (ids_AttachmentIds != null && ids_AttachmentIds.Values.Contains(attachmentId))
            {
                //更新实体缓存
                RealTimeCacheHelper.IncreaseEntityCacheVersion(ids_AttachmentIds.FirstOrDefault(n => n.Value == attachmentId).Key);
            }

            return count > 0;
        }

        /// <summary>
        /// 清理记录
        /// </summary>
        /// <param name="userId">用户Id</param>
        /// <param name="attachmentId">附件Id</param>
        /// <param name="accessType">操作种类</param>
        /// <param name="lastDate">日期期限</param>
        public void Clean(long userId = 0, long attachmentId = 0, AccessType? accessType = null, DateTime? lastDate = null)
        {
            var sql = Sql.Builder;
            sql.Append("Delete from tn_AttachmentAccessRecords");
            int whereCount = 0;
            if (userId > 0)
            {
                whereCount++;
                sql.Where("UserId=@0", userId);
            }
            if (attachmentId > 0)
            {
                whereCount++;
                sql.Where("AttachmentId=@0", attachmentId);
            }
            if (accessType.HasValue)
            {
                whereCount++;
                sql.Where("AccessType=@0", accessType);
            }
            if (lastDate.HasValue)
            {
                whereCount++;
                sql.Where("LastDownloadDate>@0", lastDate);
            }
            if (whereCount > 0)
            {
                CreateDAO().Execute(sql);
            }
        }

        #endregion Create/Update

        #region Get/Gets

        /// <summary>
        /// 根据获取用户附件下载记录及附件的Id集合
        /// </summary>
        /// <param name="userId">下载用户UserId</param>
        public Dictionary<long, long> GetIds_AttachmentIdsByUser(long userId)
        {
            string cacheKey = GetCacheKey_RecordIds_AttachmentIds(userId);

            Dictionary<long, long> ids_attachmentIds = cacheService.GetFromFirstLevel<Dictionary<long, long>>(cacheKey);

            if (ids_attachmentIds == null || ids_attachmentIds.Count == 0)
            {
                var sql = Sql.Builder;
                sql.Select("Id,AttachmentId")
                   .From("tn_AttachmentAccessRecords")
                   .Where("UserId = @0", userId);

                IEnumerable<dynamic> reuslts = CreateDAO().Fetch<dynamic>(sql);

                if (reuslts != null)
                {
                    ids_attachmentIds = reuslts.ToDictionary<dynamic, long, long>(v => v.Id, v => v.AttachmentId);
                }

                //更新缓存
                cacheService.Set(cacheKey, ids_attachmentIds, CachingExpirationType.RelativelyStable);
            }

            return ids_attachmentIds;
        }

        /// <summary>
        /// 获取附件的前topNumber条下载记录
        /// </summary>
        /// <param name="attachmentId">附件Id</param>
        /// <param name="topNumber">返回的记录数</param>
        public IEnumerable<AttachmentAccessRecords> GetTopsByAttachmentId(long attachmentId, int topNumber)
        {
            var sql = Sql.Builder;
            sql.Where("AttachmentId = @0", attachmentId);
            return GetTopEntities(topNumber, sql);
        }

        /// <summary>
        /// 获取附件的下载记录分页显示
        /// </summary>
        /// <param name="attachmentId">附件Id</param>
        /// <param name="pageIndex">页码</param>
        public PagingDataSet<AttachmentAccessRecords> GetsByAttachmentId(long attachmentId, int pageIndex)
        {
            var sql = Sql.Builder;
            sql.Where("AttachmentId = @0", attachmentId)
                .OrderBy("LastDownloadDate desc");
            return GetPagingEntities(pageSize, pageIndex, sql);
        }

        /// <summary>
        /// 获取用户的下载记录分页显示
        /// </summary>
        /// <param name="userId">下载用户UserId</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="needToBuy">是否需要购买</param>
        public PagingDataSet<AttachmentAccessRecords> GetsByUserId(long userId, int pageIndex, bool needToBuy = true)
        {
            //组装获取实体的sql语句
            var sql = Sql.Builder;
            sql.Where("UserId = @0", userId);
            if (needToBuy)
                sql.Where("Price > 0");
            else
                sql.Where("Price = 0");
            return GetPagingEntities(pageSize, pageIndex, sql);
        }

        /// <summary>
        /// 获取用户下载的附件内容Id集合
        /// </summary>
        /// <param name="userId">用户UserId</param>
        /// <param name="TenantTypeId">租户Id</param>
        /// <param name="accessType">附件操作类型（下载、浏览）</param>
        public List<long> GetAttachmentIdsByUserId(long userId, string TenantTypeId, AccessType accessType = AccessType.Download)
        {
            //组装获取实体的sql语句
            var sql = Sql.Builder;
            sql.Select("tn_Attachments.AssociateId").From("tn_AttachmentAccessRecords").LeftJoin("tn_Attachments").On("tn_AttachmentAccessRecords.AttachmentId = tn_Attachments.AttachmentId");
            var sqlWhere = Sql.Builder;
            sqlWhere.Where("tn_AttachmentAccessRecords.UserId = @0", userId);
            sqlWhere.Where("tn_Attachments.TenantTypeId=@0", TenantTypeId);
            sqlWhere.Where("tn_AttachmentAccessRecords.AccessType = @0", accessType);
            sql.Append(sqlWhere);
            sql.OrderBy("tn_AttachmentAccessRecords.DownloadDate desc");
            return CreateDAO().Fetch<long>(sql);
        }

        /// <summary>
        /// 获取拥有者附件的下载记录分页显示
        /// </summary>
        /// <param name="userId">附件拥有者Id</param>
        /// <param name="pageIndex">页码</param>
        /// <param name="needToBuy">是否需要购买</param>
        public PagingDataSet<AttachmentAccessRecords> GetsByOwnerId(long userId, int pageIndex, bool needToBuy = true)
        {
            //组装获取实体的sql语句
            var sql = Sql.Builder;
            sql.Where("UserId = @0", userId);
            if (needToBuy)
                sql.Where("Price > 0");
            else
                sql.Where("Price = 0");
            return GetPagingEntities(pageSize, pageIndex, sql);
        }

        #endregion Get/Gets

        #region GetCacheKey

        /// <summary>
        /// 获取下载记录与附件Id集合的CacheKey
        /// </summary>
        /// <param name="userId">下载用户UserId</param>
        /// <returns></returns>
        private string GetCacheKey_RecordIds_AttachmentIds(long userId)
        {
            return "RecordIds_AttachmentIds::UserId:" + userId;
        }

        #endregion GetCacheKey
    }
}